using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }
        [HttpGet("obter Venda Por Id{id}")]
        public IActionResult ObterPorId(int id)
        {
            var obtendoVenda = _context.Vendas.Find(id);
            if (obtendoVenda == null)
                return NotFound("Por favor, informe um Id válido.");
                return Ok(obtendoVenda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Vendedor venda)
        {
            if (venda.Status != EnumStatusVenda.AguardandoPagamento)
                return NotFound("O status não pode ser diferente de (0)AguardandoPagamento.");

                _context.Vendas.Add(venda);
                _context.SaveChanges();
                return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPut("Atualizar venda{id}")]
        public IActionResult AtualizarVenda(int id, Vendedor venda)
        {
            var atualizaVenda = _context.Vendas.Find(id);
            if(atualizaVenda == null)
                return NotFound("Por favor, informar um Id.");

            if (atualizaVenda.Status == EnumStatusVenda.AguardandoPagamento)
            {
                if (venda.Status != EnumStatusVenda.PagamentoAprovado && atualizaVenda.Status != EnumStatusVenda.PagamentoAprovado)
                {
                    return NotFound("O pagamento está com o status como (0)AguardandoPagamento, por favor, escolha: (1)PagamentoAprovado ou (4)Cancelada");
                }
            }
            else if (atualizaVenda.Status == EnumStatusVenda.PagamentoAprovado)
            {
                if (venda.Status != EnumStatusVenda.EnviadoParaTransportadora && atualizaVenda.Status != EnumStatusVenda.Cancelada)
                {
                    return NotFound("O pagamento está com o status como (1)PagamentoAprovado, por favor, escolha: (2)EnviadoParaTransportadora ou (4)Cancelada");
                }                
            }
            else if (atualizaVenda.Status == EnumStatusVenda.EnviadoParaTransportadora)
            {
                if (venda.Status != EnumStatusVenda.Entregue)
                {
                    return NotFound("O pagamento está com o status como (2)EnviadoParaTransportadora, por favor, escolha: (3)Entregue");
                }
            }

            atualizaVenda.Status = venda.Status;
            _context.Vendas.Update(atualizaVenda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = atualizaVenda.Id }, atualizaVenda);    
        }            
    }

}        


